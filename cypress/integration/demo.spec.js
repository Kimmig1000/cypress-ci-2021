/// <reference types="cypress" />

describe('CI CD Demo', () => {

  beforeEach(() => {
    cy.exec('yarn reset:db')
    cy.visit('/')
  })

  it('should add a todo', () => {
    cy.get('[placeholder="New Item"]')
      .clear()
      .type('Testelement');
    cy.contains('button', 'Add Item').click();
    cy.get('div.name').should('have.text', 'Testelement');
  });

});